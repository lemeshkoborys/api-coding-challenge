from django.contrib import admin
from .models import ATTSubscription


@admin.register(ATTSubscription)
class ATTSubscriptionModelAdmin(admin.ModelAdmin):
    list_display = (
        'user',
        'plan',
        'status',
        'device_id',
        'phone_number',
        'phone_model',
        'network_type',
        'effective_date',
        'created_at',
        'updated_at',
        'deleted',
    )

    exclude = (
        'activation_code',
        'created_at',
        'updated_at',
    )
