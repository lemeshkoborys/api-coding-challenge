from django.contrib.auth.models import User
from django.db import models

from .helpers import generate_activation_code


class ATTSubscription(models.Model):
    """Represents a subscription with AT&T for a user and a single device"""

    NEW = 'new'
    ACTIVE = 'active'
    EXPIRED = 'expired'

    STATUS = (
        (NEW, 'New'),
        (ACTIVE, 'Active'),
        (EXPIRED, 'Expired'),
    )

    user = models.ForeignKey(User, on_delete=models.PROTECT)  # Owning user

    plan = models.ForeignKey('plans.Plan', null=True, on_delete=models.PROTECT)
    status = models.CharField(max_length=10, choices=STATUS, default=NEW)
    activation_code = models.CharField(max_length=120, default=generate_activation_code)
    device_id = models.CharField(max_length=20, blank=True, default='')
    phone_number = models.CharField(max_length=20, blank=True, default='')
    phone_model = models.CharField(max_length=128, blank=True, default='')
    network_type = models.CharField(max_length=5, blank=True, default='')

    effective_date = models.DateTimeField(null=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    deleted = models.BooleanField(default=False)
