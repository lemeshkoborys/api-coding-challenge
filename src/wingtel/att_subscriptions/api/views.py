import datetime
from django.shortcuts import reverse
from rest_framework import viewsets, status, views
from rest_framework.permissions import IsAuthenticated
from rest_framework.mixins import RetrieveModelMixin
from rest_framework.response import Response

from wingtel.att_subscriptions.models import ATTSubscription
from wingtel.att_subscriptions.api.serializers import ATTSubscriptionSerializer
from wingtel.purchases.models import Purchase
from wingtel.att_subscriptions.helpers import preactivation_validation

from .permissions import SubscriptionOwnerPermission


class ATTSubscriptionViewSet(viewsets.ModelViewSet):
    """
    A viewset that provides `retrieve`, `create`, and `list` actions.
    """
    queryset = ATTSubscription.objects.all()
    serializer_class = ATTSubscriptionSerializer


class ATTActivateSubscriptionViewSet(RetrieveModelMixin, viewsets.GenericViewSet):
    serializer_class = ATTSubscriptionSerializer
    permission_classes = [IsAuthenticated, SubscriptionOwnerPermission, ]
    queryset = ATTSubscription.objects.all()

    def retrieve(self, request, *args, **kwargs):
        subscription = self.get_object()

        if preactivation_validation(subscription) is not True:
            error_data, error_status = preactivation_validation(subscription)
            return Response(error_data, status=error_status)

        if request.query_params['code'] == subscription.activation_code:
            subscription.status = ATTSubscription.ACTIVE
            subscription.save()
            Purchase.objects.create(
                user=subscription.user,
                status=Purchase.OVERDUE,
                payment_date=datetime.datetime.now(),
                amount=subscription.plan.price
            )
            serializer = self.serializer_class(subscription)
            return Response(serializer.data, status=status.HTTP_200_OK)

        else:
            error_response = {'activation_code':
                              'Wrong activation code or it`s corrupted'}
            return Response(error_response, status.HTTP_400_BAD_REQUEST)

