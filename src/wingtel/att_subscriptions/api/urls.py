from django.urls import path
from rest_framework import routers
from .views import ATTActivateSubscriptionViewSet, ATTSubscriptionViewSet

router = routers.DefaultRouter()

router.register('subscriptions', ATTSubscriptionViewSet, 'subscriptions')


urlpatterns = [
    path('subscriptions/<int:pk>/activate/',
         ATTActivateSubscriptionViewSet.as_view({'get': 'retrieve'}),
         name='activate_att_subscription')
] + router.urls
