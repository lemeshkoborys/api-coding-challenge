import string
import random
from rest_framework import status

NEW = 'new'


def generate_activation_code():
    return ''.join(random.SystemRandom().choice(string.ascii_letters + string.digits)
                   for _ in range(120))


def preactivation_validation(subscription):
    if not subscription.plan or not subscription.phone_number or not subscription.device_id:
        error_data = {
            'subscription':
                'Subscription must contain plan, phone_number and device_id'
        }
        return error_data, status.HTTP_400_BAD_REQUEST
    if subscription.status != NEW:
        error_data = {
            'status':
                'Subscription is already activated or expired'
        }
        return error_data, status.HTTP_400_BAD_REQUEST
    return True
