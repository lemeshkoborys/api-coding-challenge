from rest_framework import viewsets

from wingtel.plans.models import Plan
from wingtel.plans.api.serializers import PlanSerializer


class PlanViewSet(viewsets.ReadOnlyModelViewSet):
    """
    A viewset that provides `retrieve` and `list` actions.
    """
    queryset = Plan.objects.all()
    serializer_class = PlanSerializer
