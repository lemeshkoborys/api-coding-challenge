from django.contrib.auth.models import User
from django.db import models


class Purchase(models.Model):
    """Represents a purchase for a user and their subscription(s)"""
    PENDING = 'pending'
    OVERDUE = 'overdue'
    COMPLETE = 'complete'
    CANCELLED = 'cancelled'

    STATUS = (
        (PENDING, 'Pending'),
        (OVERDUE, 'Past Due'),
        (COMPLETE, 'Complete'),
        (CANCELLED, 'Cancelled'),
    )

    user = models.ForeignKey(User, on_delete=models.PROTECT)

    status = models.CharField(max_length=20, choices=STATUS, default=PENDING)
    amount = models.DecimalField(max_digits=8, decimal_places=2)
    payment_date = models.DateTimeField(null=True, db_index=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
