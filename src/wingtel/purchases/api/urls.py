from django.urls import path
from rest_framework.routers import DefaultRouter
from .views import PurchaseViewSet

router = DefaultRouter()
router.register('', PurchaseViewSet, 'purchases')

urlpatterns = [] + router.urls
