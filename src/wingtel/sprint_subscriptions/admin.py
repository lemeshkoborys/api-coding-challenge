from django.contrib import admin
from .models import SprintSubscription


@admin.register(SprintSubscription)
class SprintSubscriptionModelAdmin(admin.ModelAdmin):
    list_display = (
        'user',
        'plan',
        'status',
        'device_id',
        'phone_number',
        'phone_model',
        'sprint_id',
        'effective_date',
        'created_at',
        'updated_at',
        'deleted',
    )

    exclude = (
        'created_at',
        'updated_at',
        'deleted',
    )
