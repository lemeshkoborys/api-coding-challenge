from django.urls import path
from rest_framework.routers import DefaultRouter
from .views import SprintSubscriptionViewSet, SprintActivateSubscriptionViewSet

router = DefaultRouter()

router.register(r'subscriptions', SprintSubscriptionViewSet)

urlpatterns = [
    path('subscriptions/<int:pk>/activate/',
         SprintActivateSubscriptionViewSet.as_view({'get': 'retrieve'}),
         name='sprint_subscription_activate'),
] + router.urls
