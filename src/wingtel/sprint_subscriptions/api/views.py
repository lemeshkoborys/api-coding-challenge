import datetime
from rest_framework import viewsets, status
from rest_framework.response import Response
from rest_framework.mixins import RetrieveModelMixin
from rest_framework.permissions import IsAuthenticated

from wingtel.sprint_subscriptions.models import SprintSubscription
from wingtel.sprint_subscriptions.api.serializers import SprintSubscriptionSerializer
from wingtel.att_subscriptions.api.permissions import SubscriptionOwnerPermission
from wingtel.att_subscriptions.helpers import preactivation_validation
from wingtel.purchases.models import Purchase


class SprintSubscriptionViewSet(viewsets.ModelViewSet):
    """
    A viewset that provides `retrieve`, `create`, and `list` actions.
    """
    queryset = SprintSubscription.objects.all()
    serializer_class = SprintSubscriptionSerializer


class SprintActivateSubscriptionViewSet(RetrieveModelMixin, viewsets.GenericViewSet):
    serializer_class = SprintSubscriptionSerializer
    queryset = SprintSubscription.objects.all()
    permission_classes = [IsAuthenticated, SubscriptionOwnerPermission, ]

    def retrieve(self, request, *args, **kwargs):
        subscription = self.get_object()

        if preactivation_validation(subscription) is not True:
            error_data, error_status = preactivation_validation(subscription)
            return Response(error_data, status=error_status)

        if request.query_params['code'] == subscription.activation_code:
            subscription.status = SprintSubscription.ACTIVE
            subscription.save()
            Purchase.objects.create(
                user=subscription.user,
                status=Purchase.OVERDUE,
                payment_date=datetime.datetime.now(),
                amount=subscription.plan.price
            )
            serializer = self.serializer_class(subscription)
            return Response(serializer.data, status=status.HTTP_200_OK)

        else:
            error_response = {'activation_code':
                              'Wrong activation code or it`s corrupted'}
            return Response(error_response, status.HTTP_400_BAD_REQUEST)
